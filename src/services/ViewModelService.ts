import {ViewModel} from "@/types/ViewModel";

export default class ViewModelService {
    private static get KEY() {
        return 'ViewModel';
    }

    public getEmptyViewModel(): ViewModel {
        return {
            query: {
                guild: {
                    name: '',
                    realm: ''
                }
            },
            filter: {
                guild: {
                    ranks: ["7", "6", "5", "4"]
                },
                level: {
                    start: 1,
                    end: 60
                }
            },
            ranks: {
                0: "Guild Master",
                1: "Rank 1",
                2: "Rank 2",
                3: "Rank 3",
                4: "Rank 4",
                5: "Rank 5",
                6: "Rank 6",
                7: "Rank 7",
                8: "Rank 8",
                9: "Rank 9"
            },
            sort: {
                by: undefined,
                ascending: true
            },
            rules: {
                promotion: []
            }
        };
    }

    public saveViewModel(vm: ViewModel) {
        const json = JSON.stringify(vm);
        localStorage.setItem(ViewModelService.KEY, json);
    }

    public loadViewModel(): ViewModel {
        const json = localStorage.getItem(ViewModelService.KEY);
        if (json)
            return JSON.parse(json);

        return this.getEmptyViewModel();
    }

    public addPromotionRule(vm: ViewModel) {
        vm.rules.promotion.push(ViewModelService.getEmptyPromotionRule());
    }

    private static getEmptyPromotionRule() {
        return {
            rank: 9,
            level: 1,
            itemLevel: 0,
            pve: {
                raid: 'N',
                io: 0
            },
            pvp: {
                honorLevel: 0,
                rating: 0
            }
        };
    }
}
