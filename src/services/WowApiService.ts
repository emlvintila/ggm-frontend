import {formatString, slugify} from '@/utils/utils';
import axios, {AxiosInstance} from 'axios';
import {GuildRosterAPIResponse} from '@/types/Guild';
import {PvpBracketAPIResponse, PvpSummaryAPIResponse} from '@/types/Pvp';

export default class WowApiService {
    private static get DEFAULT_LOCALE() {
        return 'en_US';
    }

    private static get NUM_ITEMS() {
        return 16;
    }

    private wow: AxiosInstance;

    constructor() {
        this.wow = axios.create({
            baseURL: 'https://localhost:5001/wow/',
            timeout: 9999999,
        });
    }

    public async callWowApi(url: string, ...args: string[]) {
        args = args.map(slugify);
        const formattedUrl = formatString(url, ...args);

        const urlObj = new URL(formattedUrl);
        if (!urlObj.searchParams.has('namespace'))
            urlObj.searchParams.append('namespace', 'profile-us');
        if (!urlObj.searchParams.has('locale'))
            urlObj.searchParams.append('locale', WowApiService.DEFAULT_LOCALE);

        const encodedUrl = encodeURIComponent(urlObj.toString());

        return (await this.wow.get(encodedUrl)).data;
    }

    public async getGuildRoster(realm: string, guild: string): Promise<GuildRosterAPIResponse> {
        return await this.callWowApi('https://us.api.blizzard.com/data/wow/guild/{0}/{1}/roster', realm, guild);
    }

    public async getPvpSummary(realm: string, character: string): Promise<PvpSummaryAPIResponse> {
        return await this.callWowApi('https://us.api.blizzard.com/profile/wow/character/{0}/{1}/pvp-summary', realm, character);
    }

    public async getPvpBracket(realm: string, character: string, bracket: '2v2' | '3v3' | 'rbg'): Promise<PvpBracketAPIResponse> {
        return await this.callWowApi('https://us.api.blizzard.com/profile/wow/character/{0}/{1}/pvp-bracket/{2}', realm, character, bracket);
    }
}
