import {formatString, slugify} from '@/utils/utils';
import {RaiderIoAPIResponse} from '@/types/Pve';
import axios, {AxiosInstance} from 'axios';

export default class RaiderIoService {
    private raiderIo: AxiosInstance;

    constructor() {
        this.raiderIo = axios.create({
            baseURL: 'https://raider.io/api/v1/',
            timeout: 9999999,
        });
    }

    public async callRaiderIoApi(url: string, ...args: string[]) {
        args = args.map(slugify);
        const formattedUrl = formatString(url, ...args);

        return (await this.raiderIo.get(formattedUrl)).data;
    }

    public async getRaiderIoData(realm: string, character: string): Promise<RaiderIoAPIResponse> {
        return this.callRaiderIoApi(`characters/profile?region=us&realm={0}&name={1}&fields=gear,raid_progression%2Cmythic_plus_scores_by_season%3Acurrent`, realm, character);
    }
}
