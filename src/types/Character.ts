export type RealmBase = {
    key: {
        href: string;
    };
    id: number;
    slug: string;
};

export type CharacterBase = {
    key: {
        href: string;
    };
    name: string;
    id: number;
    realm: RealmBase;
};
