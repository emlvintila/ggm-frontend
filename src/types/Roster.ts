import {GuildMember} from '@/types/Guild';
import {RealmBase} from '@/types/Character';
import {Bracket, HonorLevel} from '@/types/Pvp';
import {RaiderIoRaid} from '@/types/Pve';

export type RosterMember = GuildMember & {
    character: {
        realm: RealmBase & {
            name?: string;
        };
    };
    item_level: number;
} & HonorLevel & {
    topBracket: Bracket;
} & {
    io: number;
} & {
    raid: RaiderIoRaid;
} & {
    eligible_promotion?: {
        rank: number;
    };
};

export type Roster = {
    guild: {
        name: string;
        realm: string;
    };
    members: {
        [id: string]: RosterMember;
    };
};
