import {CharacterBase} from '@/types/Character';

export type GuildMember = {
    character: CharacterBase & {
        level: number;
        playable_class: {
            key: {
                href: string;
            };
            id: number;
        };
        playable_race: {
            key: {
                href: string;
            };
            id: number;
        };
    };
    rank: number;
};

export type GuildRosterAPIResponse = {
    members: GuildMember[];
};
