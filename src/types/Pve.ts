export type RaiderIoRaid = {
    summary: string;
    total_bosses: number;
    normal_bosses_killed: number;
    heroic_bosses_killed: number;
    mythic_bosses_killed: number;
};

export type RaiderIoAPIResponse = {
    realm: string;
    gear: {
        item_level_equipped: number;
    };
    mythic_plus_scores_by_season: {
        season: string;
        scores: {
            all: number;
        };
    }[];
    raid_progression: {
        [raid: string]: RaiderIoRaid;
    };
};
