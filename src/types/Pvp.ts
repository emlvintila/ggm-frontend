import {CharacterBase} from "@/types/Character";

export type HonorLevel = {
    honor_level: number;
};

export type BracketName = '2v2' | '3v3' | 'rbg' | 'N/A';

export type Bracket = {
    name: BracketName;
    rating: number;
};

export type PvpSummaryAPIResponse = HonorLevel & {
    brackets: {
        href: string;
    }[];
    character: CharacterBase;
};

export type PvpBracketAPIResponse = {
    _links: {
        self: {
            href: string;
        };
    };
    character: CharacterBase;
    rating: number;
    season: {
        id: number;
    };
};
