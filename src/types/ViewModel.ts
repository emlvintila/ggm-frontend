import {RosterMember} from '@/types/Roster';

type PromotionRule = {
    rank: number;
    level: number;
    itemLevel: number;
    pvp: { honorLevel: number; rating: number };
    pve: { io: number; raid: string };
};

type Rules = {
    promotion: PromotionRule[];
};

export type ViewModel = {
    query: {
        guild: {
            name: string;
            realm: string;
        };
    };
    filter: {
        guild: {
            ranks: string[];
        };
        level: {
            start: number;
            end: number;
        };
    };
    sort: {
        by?: keyof RosterMember;
        ascending: boolean;
    };
    ranks: {
        [rank: number]: string;
    };
    rules: Rules;
};
