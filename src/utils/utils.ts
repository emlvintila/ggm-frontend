export function slugify(str: string): string {
    return str.toLowerCase().replaceAll(/[']+/g, '').replaceAll(/\s+/g, '-');
}

export function formatString(format: string, ...args: string[]): string {
    for (let i = 0; i < args.length; i++) {
        format = format.replaceAll(`{${i}}`, args[i]);
    }

    return format;
}
